\documentclass{llncs}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[scaled=0.8]{beramono}

\usepackage{graphicx}
\usepackage{color}
\usepackage{booktabs}

\usepackage{hyperref}
\hypersetup{hidelinks,
    colorlinks=true,
    allcolors=blue,
    pdfstartview=Fit,
    pdfusetitle=true,
    breaklinks=true}

\newcommand{\TODO}[1]{{\color{red}#1}}

% Insert the name of "your journal" with
% \journalname{myjournal}

\begin{document}

\title{The ARCH-COMP Friendly Competition%\thanks{Grants or other notes
%about the article that should go on the front page should be
%placed here. General acknowledgments should be placed at the end of the article.}
}
% \subtitle{Do you have a subtitle?\\ If so, write it here}

%\titlerunning{Short form of title}        % if too long for running head

% TODO: add your name here (alphabetical order?)
\author{
        \ldots \and
    Gidon Ernst \inst{1} \and
        \ldots
}

%\authorrunning{Short form of author list} % if too long for running head

\institute{LMU Munich, Germany \\
           \email{gidon.ernst@lmu.de}
}

% \date{Received: date / Accepted: date}
% The correct dates will be entered by the editor


\maketitle

\begin{abstract}
The workshop on Applied Verification for Continuous and Hybrid Systems (ARCH)
is an annual venue bringing together researchers and practitioners
in the area of automated hybrid systems analysis and verification.
Associated to the event is a \emph{friendly competition}, ARCH-COMP,
in which methods and tools are compared in an evaluation on benchmark problems.
The competition was established in 2017 and aims to explore, document, and push forward the state-of-the-art.
It is supported by an active community around several categories of problems,
classified e.g. by linear vs. nonlinear dynamics, simulation-based vs. analytic methods,
and application domains such as automotive systems or neural networks.
This paper describes the format of the competition and its organization.
\keywords{hybrid systems \and competition \and tool evaluation}
% \PACS{PACS code1 \and PACS code2 \and more}
% \subclass{MSC code1 \and MSC code2 \and more}

\textbf{Website:} \url{https://cps-vo.org/group/ARCH/FriendlyCompetition}
\end{abstract}

\section{Introduction}
\label{sec:introduction}

\TODO{From the website:}
This workshop addresses verification techniques for continuous and hybrid systems with a special focus on the transfer from theory to practice. It maintains a curated set of verification benchmarks submitted by academia and industry. The ARCH workshop also hosts the ARCH-COMP friendly competition, which provides an up-to-date snapshot of the state of the art in verification tools in a large variety of categories, including,
Formal Verification of Hybrid Systems (piecewise constant, linear, and nonlinear dynamics),
Artificial Intelligence and Neural Network Control Systems,
Stochastic Models,
Falsification, and
Hybrid Programs and Theorem Proving.

\begin{itemize}
\item Quick overview of background:
      hybrid systems verification, models, tools, analysis goals, approaches
\item ARCH workshop history
\item goals of the competition
\item remark on the word ``friendly''
\item reference some previous reports
\item website, repository
\end{itemize}

\section{Organization and Competition Format}
\label{sec:organization}

top-down description
\begin{itemize}
\item competition associated to ARCH workshop
\item several categories + brief descriptions
\item overview of the schedule each year
\item overview of the work done in each group
\item describe artifacts that are produced concretely
\item describe how results are reported in the papers (e.g. plots, tables)
\item comparison to some other competitions
\end{itemize}

\section{Category Descriptions}

This section could document the goals and ideas behind each category
\begin{itemize}
\item what dynamics, models, properties
\item participants over the years, with lots of references
\item what was new in each year, what were the main achievements? relate to goals outlined above
\end{itemize}

\subsection{Falsification}

The falsification category targets the black-/greybox analysis of
executable models with respect to requirements expressed in
temporal logic with time bounds, encoded in MTL \cite{} or STL \cite{}.
The task is to find initial conditions and time-varying inputs
subject to given constraints that steer the system into
a violation of the respective requirement.
Typical approaches are simulation-based and employ quantitative metrics \cite{}
of how close a given input is to violating a requirement (``robustness semantics'').
Research in this area has produced a variety of techniques, mature tools, and practical applications;
these are described in overview survey articles \cite{}.
For past instalments of this competition 2017–2020 see \cite{}.

Synopsis
\begin{itemize}
\item Problem solved: finding counterexamples to requirements by simulations
\item Property/Specification format: Signal/Metric Temporal Logic formulas
\item Model format: executable hybrid systems, e.g. written in Matlab, Simulink, or Python
\item Methods used: stochastic optimization, semi-random exploration, surrogate models, abstraction
\item Performance measures: Number of simulations required for falsification, success rate wrt. a given maximal simulation number and trials
\end{itemize}

\TODO{Add references and websites, shorten somewhat?}

Participants over the years are (in alphabetical order):
\begin{itemize}
\item ARIsTEO~\cite{} is a Matlab toolbox for falsification.
      It is based on black-box testing to construct a surrogate model
      in iterative approximation-refinement loop.
\item Breach~\cite{} is a Matlab toolbox for test case generation,
      formal specification, monitoring and optimization-based falsification
      and mining of requirements for hybrid dynamical systems.
\item FalCAuN~\cite{} is an experimental tool for testing a Simulink model using
      black-box checking \cite{} an automated testing method based
      on active automata learning and model checking in terms of a discrete abstraction.
\item falsify~\cite{} is an experimental program which solves falsification problems
      of safety properties by reinforcement learning\cite{}.
      falsify uses a grey-box method, that is, it learns system
      behavior by observing system outputs during simulation.
\item FalStar~\cite{} is an a falsification tool that constructs falsifying inputs
      incrementally in time, hereby exploiting potential time-causal dependencies in the problem, 
      and with gradually finer-grained samples.
\item ForeSee is a falsification tool
      based on a new robustness definition, called QB-Robustness,
      which combines quantitative robustness and classical Boolean satisfaction,
      to mitigate the problem that the robustness of subformulas is often incomparable due to different scales of magnitude.
\item S-TaLiRo\cite{} is a Matlab toolbox for monitoring and test case generation against system specifications presented in STL (or MTL).
      The test cases are automatically generated using optimization techniques guided by formal requirements in STL in order to find falsifying systems behaviors,
      notably the Stochastic Optimization with Adaptive Restarts (SOAR) \cite{} framework.
\item  zlscheck is a tool for test case generation of programs written in Zélus\cite{},
       a language reminiscent of the synchronous languages Lustre and Scade extended in order to express ODEs.
\end{itemize}

The competition's benchmark repository has been gradually extended over the years,
now comprising seven system models, realized in Matlab/Simulink,
with a total of $\approx$30 requirements, formalized in STL/MTL.
The models encompass a variety of difficulties, such as nonlinear or discontinuous behavior,
and combinatorially large search spaces.

The main effort in the earlier years was related to compile the benchmark suite
as well as results from all participants for all benchmark tasks.
Results were reported by the participating teams on a basis of trust,
however, it became clear that there is significant room for inadertedly introducing errors in the experimental setup.
In 2021 therefore, falsifying input traces were collected and validated independently
(using the FalStar tool).
This uncovered a number of errors, such as numerical discrepancies in the simulation
due to differences in Matlab versions and/or model configurations,
but also due to errors in formalizing the requirements, inconcsistencies in the documentation thereof,
and issues with the data format used for transferring the results.
Most of the mistakes and systematic issues could be resolved and the competition of 2022
presents a comprehensive validated set of results.

\section{Overall Achievements}

\begin{itemize}
\item benchmark repository
\item common formats
\item baseline for novel approaches in research
\item pushing forward state-of-the-art
\item mention the award, possibly list all award winners
\item repeatability evaluation
\item visibility (e.g. in industry)
\end{itemize}

\section{Outlook}

\begin{itemize}
\item think about what goes well now
\item what can be improved? pick specific aspects that we like to mention
\item how to participate or contribute benchmarks
\end{itemize}

\end{document}
